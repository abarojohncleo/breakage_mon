<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Incident;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $incidents = Incident::where('is_archived', 0)->get();
        return view('incidents.index', [
            'incidents' => $incidents
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('incidents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incident = new Incident;

        $incident->name = $request->name;
        $incident->grade_and_section = $request->grade_and_section;
        $incident->group = $request->group;
        $incident->teacher_in_charge = $request->teacher_in_charge;
        $incident->quantity = $request->quantity;
        $incident->item_name = $request->item_name;
        $incident->contact = $request->contact;
        $incident->description = $request->description;
        $incident->date_of_replacement = $request->date_of_replacement;

        $incident->save();
        $request->session()->flash('message', 'Incident Added.');

        return redirect('/incidents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incident = Incident::find($id);
        return view('incidents.edit',[
            'incident' => $incident
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $incident = Incident::find($id);

        $incident->name = $request->name;
        $incident->grade_and_section = $request->grade_and_section;
        $incident->group = $request->group;
        $incident->teacher_in_charge = $request->teacher_in_charge;
        $incident->quantity = $request->quantity;
        $incident->item_name = $request->item_name;
        $incident->contact = $request->contact;
        $incident->description = $request->description;
        $incident->date_of_replacement = $request->date_of_replacement;

        $incident->save();
        $request->session()->flash('message', 'Incident information has been updated.');

        return redirect('/incidents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $incident = Incident::find($id);

        $incident->date_replaced = date('Y/m/d');
        $incident->incident_status = 'Item replaced';
        $incident->is_archived = 1;
        $incident->save();

        $request->session()->flash('message','The incident has been deleted');

        return redirect('/incidents');
    }

    public function deleteConfirm($id)
    {
        $incident = Incident::find($id);
        return view('incidents.delete', [
            'incident' => $incident
        ]);
    }
}
