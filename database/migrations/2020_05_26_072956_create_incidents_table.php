<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150);
            $table->string('grade_and_section', 150);
            $table->longtext('group');
            $table->string('teacher_in_charge', 50);
            $table->unsignedBigInteger('quantity');
            $table->string('item_name', 50);
            $table->string('contact', 50);
            $table->longtext('description');
            $table->longtext('date_of_replacement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidents');
    }
}
