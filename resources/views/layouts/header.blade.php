<nav class="navbar navbar-expand-lg navbar-dark " style="background-color: #093916;">
	<a class="navbar-brand" href="">BreakagemonApp</a>
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse text-right" id="navbar">
		<ul class="navbar-nav mr-auto">
			
			<li class="nav-item">
				<a class="nav-link" href="{{ url('/incidents') }}">Incident List</a>
			</li>
			
		</ul>

		<ul class="navbar-nav ml-auto">
			@guest
				<li class="nav-item">
					<a href="{{ url('/login') }}" class="nav-link">Login</a>
				</li>
			@else

				@if(!empty(Auth::user()) && Auth::user()->user_role == 'admin')
				
				<li class="nav-item">
					<a class="nav-link" href="{{url('/histories')}}">
						Histories
					</a>
				</li>

				@endif

				<li class="nav-item">
					<a class="nav-link" href="{{url('/')}}" onclick="document.querySelector('#logout-form').submit()">Logout</a>
				</li>
			@endguest
		</ul>
	</div>
</nav>

<form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
	@csrf
</form>