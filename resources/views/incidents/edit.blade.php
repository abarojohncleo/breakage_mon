@extends('layouts.app')

@section('title', 'Edit Incident')

@section('edit-incident-form')

	<form action='{{ url("incidents/$incident->id") }}' method="POST" enctype="multipart/form-data">

		@csrf
		@method("PUT")

		<div class="form-group">
			<label>Student Name</label>
			<input type="text" name="name" class="form-control" value="{{$incident->name}}" required>
		</div>

		<div class="form-group">
			<label>Student Grade and Section</label>
			<input type="text" name="grade_and_section" class="form-control" value="{{$incident->grade_and_section}}" required>
		</div>

		<div class="form-group">
			<label>Contact through</label>
			<input type="text" name="contact" class="form-control" value="{{$incident->contact}}" required>
		</div>

		<div class="form-group">
			<label>Teacher In Charge</label>
			<input type="text" name="teacher_in_charge" class="form-control" value="{{$incident->teacher_in_charge}}" required>
		</div>

		<div class="form-group">
			<label>Item name</label>
			<input type="text" name="item_name" class="form-control" value="{{$incident->item_name}}" required>
		</div>

		<div class="form-group">
			<label>Group Members</label>
			<input type="text" name="group" class="form-control" value="{{$incident->group}}" required>
		</div>

		<div class="form-group">
			<label>Quantity</label>
			<input type="number" name="quantity" class="form-control" value="{{$incident->quantity}}" required>
		</div>


		<div class="form-group">
			<label>Description of Incident</label>
			<input type="text" name="description" class="form-control" value="{{$incident->description}}" required>
		</div>

		<div class="form-group">
			<label>Due Date</label>
			<input type="text" name="date_of_replacement" class="form-control" value="{{$incident->date_of_replacement}}" required>
		</div>

		<button type="submit" class="btn btn-success ">Update</button>
		<a href="{{ url('/incidents') }}" class="btn btn-warning">Cancel</a>
		
	</form>
@endsection
@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Edit Incident</h3>
				<div class="card">
					<div class="card-header">Incident Information</div>
					<div class="card-body">
						@yield('edit-incident-form')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection	