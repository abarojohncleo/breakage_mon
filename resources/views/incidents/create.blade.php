@extends('layouts.app')

@section('title', 'Add Incident')

@section('add-incident-form')

    <form action="{{ url('incidents/store') }}" method="post" enctype="multipart/form-data">

		@csrf
		
		<div class="form-group">
			<label>Student Name</label>
			<input type="text" name="name" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Student Grade and Section</label>
			<input type="text" name="grade_and_section" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Contact through</label>
			<input type="text" name="contact" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Group Members</label>
			<input  type="text" name="group" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Teacher In Charge</label>
			<input type="text" name="teacher_in_charge" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Item name</label>
			<input type="text" name="item_name" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Quantity</label>
			<input type="number" name="quantity" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Description of Incident</label>
			<input type="text" name="description" class="form-control" required>
		</div>

		<div class="form-group">
			<label>Due Date</label>
			<input type="text" name="date_of_replacement" class="form-control" required>
		</div>

		<button type="submit" class="btn btn-success btn-block">Add</button>

	</form>
	
@endsection

@section('content')
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 mx-auto">
				<h3 class="text-center">Add Incident</h3>
				<div class="card">
					<div class="card-header">Incident Information</div>
					<div class="card-body">
						@yield('add-incident-form')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection