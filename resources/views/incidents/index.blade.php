@extends('layouts.app')

@section('title', 'Incident List')

@section('content')

	<div class="container-fluid">
		<h3>Incident Lists</h3>
		<blockquote style="font-style: italic;">"Be reminded that items must be replaced <strong>30 days</strong> after incident , failure to comply will result to class responsibility."</blockquote>

		@if(!empty(Auth::user()) && Auth::user()->user_role == 'admin')
			<a href="{{ url('incidents/create') }}" class="btn btn-warning my-2">Add Incident</a>
		@endif
		
	@foreach ($incidents as $incident)
		<p>
		  <button class="btn btn-block m-0" type="button" data-toggle="collapse" data-target="#collapse-{{ $incident['id'] }}" aria-expanded="false" aria-controls="collapseExample" style="background-color: lightgreen;">
		    {{ date_format(date_create($incident->created_at), "M d, Y - h:i:s A") }} <br>"<span><strong>{{$incident->name}}</strong></span>"<br>
		    <span><em>Due Date: {{$incident->date_of_replacement}}</em></span>
		  </button>
		</p>
		<div class="collapse" id="collapse-{{$incident['id']}}">
		  <div class="card card-body">
			<table class="table table-striped text-left">
			  <thead>
			    <tr>
			      <th>Details</th>
					@if(!empty(Auth::user()) && Auth::user()->user_role == 'admin')
						<th>Actions :<br>
							<a class="btn btn-outline-success my-0" href='{{ url("incidents/$incident->id/edit") }}'>Edit</a>
							<a class="btn btn-outline-danger my-0" href='{{ url("incidents/$incident->id/delete-confirm") }}'>Item Replaced/Paid</a>
						</th>
					@endif
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>Incident #: </td>
			      <td>{{$incident->id}}</td>
			    </tr>
			    <tr>
			      <td>Student Name:</td>
			      <td>{{$incident->name}}</td>
			    </tr>
			    <tr>
			      <td>Grade and Section :</td>
			      <td>{{$incident->grade_and_section}}</td>
			    </tr>
			    <tr>
			      <td>Group Members :</td>
			      <td>{{$incident->group}}</td>
			    </tr>
			    <tr>
			      <td>Teacher in Charge :</td>
			      <td>{{$incident->teacher_in_charge}}</td>
			    </tr>
			    <tr>
			      <td>Item Name :</td>
			      <td>{{$incident->item_name}}</td>
			    </tr>
			    <tr>
			      <td>Item Quantity :</td>
			      <td>{{$incident->quantity}}</td>
			    </tr>
			    <tr>
			      <td>Event of Incident :</td>
			      <td>{{$incident->description}}</td>
			    </tr>
			    <tr>
			      <td>Student Contact :</td>
			      <td>{{$incident->contact}}</td>
			    </tr>
			    <tr>
			      <td>Date of Incident: </td>
			      <td>{{$incident->created_at}}</td>
			    </tr>
			    <tr>
			      <td>Due Date :</td>
			      <td>{{$incident->date_of_replacement}}</td>
			    </tr>

			  </tbody>
			</table>
		  </div>
		</div>
	@endforeach

	</div>

@endsection

@if (!empty(session()->get('message')))
	<script type="text/javascript">alert('{{session()->get("message")}}')</script>
@endif

