@extends('layouts.app')

@section('title', 'Incident List')

@section('content')

	<div class="container-fluid">
		<h3>Incident Histories</h3>
		
		<table class="table table-striped text-left">

			<thead>
				<tr>
				<th>Student Name</th>
				<th>Item Name</th>
				<th>Quantity</th>
				<th>Due Date</th>
				<th>Incidents Status</th>
				<th>Date Replaced (Y/m/d)</th>
				</tr>
			</thead>

			@foreach ($incidents as $incident)
				<tbody>
					<tr>
						<td>{{$incident->name}}</td>
						<td>{{$incident->item_name}}</td>
						<td>{{$incident->quantity}}</td>
						<td>{{$incident->date_of_replacement}}</td>
						<td>{{$incident->incident_status}}</td>
						<td>{{$incident->date_replaced}}</td>
					</tr>
				</tbody>
			@endforeach

		</table>
	</div>


@endsection

