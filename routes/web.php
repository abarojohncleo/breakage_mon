<?php

use Illuminate\Support\Facades\Route;
use App\Incident;

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/incidents','IncidentController@index');

Route::get('/incidents/create', 'IncidentController@create');
Route::post('/incidents/store', 'IncidentController@store');

Route::get('/incidents/{id}/edit', 'IncidentController@edit');
Route::put('/incidents/{id}', 'IncidentController@update');

Route::get('/incidents/{id}/delete-confirm', 'IncidentController@deleteConfirm');
Route::delete('/incidents/{id}', 'IncidentController@destroy');

Route::get('/histories', function () {
	$incidents = Incident::all();
    return view('histories',[
    	'incidents' => $incidents
    ]);
});




